package com.tcc.moodle;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MoodleConnection {
    
    private Connection connection;
    
    public void open() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:8889/moodle30?user=root&password=root");
        } catch (SQLException ex) {
            Logger.getLogger(MoodleConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getConnection() {
        return connection;
    }
    
    public void close(){
        if(connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoodleConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
