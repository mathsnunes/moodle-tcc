package com.tcc.moodle.courses;

import com.tcc.moodle.PageConstants;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ManagementCoursePage {

    private final WebDriver driver;
    private List<WebElement> categories;

    public ManagementCoursePage(WebDriver driver) {
        this.driver = driver;
    }
    
    public void access(){
        driver.get(PageConstants.COURSE_MANAGEMENT_URL);
    }
    
    private void fetchCategoriesList(){
        categories = driver.findElements(By.className("listitem-category"));   
    }

    public List<WebElement> getCategories() {
        
        if(categories == null) {
            fetchCategoriesList();
        }
        
        return categories;
    }
    
    public WebElement getLastCategory(){
        int categoriesSize = getCategories().size();
        return getCategories().get(categoriesSize - 1);
    }
    
}
