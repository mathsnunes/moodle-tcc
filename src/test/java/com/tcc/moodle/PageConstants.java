package com.tcc.moodle;


public class PageConstants {
    public static final String LOGIN_URL = "http://localhost:8888/moodle30/login/index.php?lang=pt_br";
    public static final String MAIN_URL = "http://localhost:8888/moodle30/my/";
    public static final String ADD_CATEGORY_URL = "http://localhost:8888/moodle30/course/editcategory.php?parent=0&lang=pt_br";
    public static final String COURSE_MANAGEMENT_URL = "http://localhost:8888/moodle30/course/management.php";
}
