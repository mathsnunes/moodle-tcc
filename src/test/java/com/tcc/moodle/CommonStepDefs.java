package com.tcc.moodle;

import com.tcc.moodle.login.LoginPage;
import cucumber.api.java.pt.Dado;
import org.openqa.selenium.WebDriver;

public class CommonStepDefs {

    private final WebDriver driver;
    private final LoginPage loginPage;

    public CommonStepDefs(SharedDriver driver) {
        this.driver = driver;
        loginPage = new LoginPage(driver);
    }

    @Dado("^que estou autenticado como administrador$")
    public void que_estou_autenticado_como_administrador() throws Throwable {
        loginPage.access();
        loginPage.withUsername("admin")
                 .withPassword("Mo12345678_")
                 .login();
    }

}
