package com.tcc.moodle.login;

import com.tcc.moodle.PageConstants;
import com.tcc.moodle.SharedDriver;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class LoginStepDefs {

    private final WebDriver driver;
    private final LoginPage loginPage;

    public LoginStepDefs(SharedDriver driver) {
        this.driver = driver;
        loginPage = new LoginPage(driver);
    }

    @Dado("^que estou na pagina de login$")
    public void que_estou_na_pagina_de_login() throws Throwable {
        loginPage.access();
    }

    @Quando("^eu clicar no botao de login$")
    public void eu_clicar_no_botao_de_login() throws Throwable {
        loginPage.login();
    }

    @Entao("^a mensagem \"([^\"]*)\" deve ser exibida$")
    public void a_mensagem_deve_ser_exibida(String mensagem) throws Throwable {
        Assert.assertEquals(mensagem, loginPage.getErrorMessage());
    }

    @Dado("^o usuario é \"([^\"]*)\"$")
    public void o_usuario_é(String username) throws Throwable {
        loginPage.withUsername(username);
    }

    @Dado("^a senha é \"([^\"]*)\"$")
    public void a_senha_é(String password) throws Throwable {
        loginPage.withPassword(password);
    }

    @Entao("^devo ser redirecionado para a pagina principal$")
    public void devo_ser_redirecionado_para_a_pagina() throws Throwable {
        Assert.assertEquals(PageConstants.MAIN_URL, driver.getCurrentUrl());
    }

}
