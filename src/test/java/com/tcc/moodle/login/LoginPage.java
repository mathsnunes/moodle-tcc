package com.tcc.moodle.login;

import com.tcc.moodle.PageConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

// Classe que gerencia as ações da página de login
public class LoginPage {
    private final WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void access(){
        driver.get(PageConstants.LOGIN_URL);
    }
    
    public LoginPage withUsername(String username){
        driver.findElement(By.id("username")).sendKeys(username);
        return this;
    }
    
    public LoginPage withPassword(String password) {
        driver.findElement(By.id("password")).sendKeys(password);
        return this;
    }
    
    public String getErrorMessage(){
        return driver.findElement(By.id("loginerrormessage")).getAttribute("textContent");
    }
    
    public void login(){
        driver.findElement(By.id("loginbtn")).click();
    }
    
    public void goToForgotPasswordPage(){
        driver.findElement(By.xpath("//a[@href='forgot_password.php']")).click();
    }
    
}
