package com.tcc.moodle.category;

import com.tcc.moodle.PageConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class InsertCategoryPage {
    private final WebDriver driver;

    public InsertCategoryPage(WebDriver driver) {
        this.driver = driver;
    }
    
    public void access(){
        driver.get(PageConstants.ADD_CATEGORY_URL);
    }
    
    public InsertCategoryPage withName(String name) {
        driver.findElement(By.id("id_name")).sendKeys(name);
        return this;
    }
    
    public InsertCategoryPage withID(String id) {
        driver.findElement(By.id("id_idnumber")).sendKeys(id);
        return this;
    }
    
    public void save(){
        driver.findElement(By.id("id_submitbutton")).click();
    }
    
    public String getErrorMessageOnCategoryName(){
        WebElement categoryName = driver.findElement(By.id("fitem_id_name"));
        return categoryName.findElement(By.className("error")).getAttribute("textContent");
    }
    
    public String getErrorMessageOnCategoryId(){
        WebElement categoryName = driver.findElement(By.id("fitem_id_idnumber"));
        return categoryName.findElement(By.className("error")).getAttribute("textContent");
    }
    
}
