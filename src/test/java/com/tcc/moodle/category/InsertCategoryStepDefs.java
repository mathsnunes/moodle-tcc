package com.tcc.moodle.category;

import com.tcc.moodle.SharedDriver;
import com.tcc.moodle.courses.ManagementCoursePage;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class InsertCategoryStepDefs {

    private final WebDriver driver;
    private final InsertCategoryPage categoryPage;
    private final ManagementCoursePage managementCoursePage;

    public InsertCategoryStepDefs(SharedDriver driver) {
        this.driver = driver;
        categoryPage = new InsertCategoryPage(driver);
        managementCoursePage = new ManagementCoursePage(driver);
    }

    @Dado("^estou na pagina de cadastro de categoria$")
    public void estou_na_pagina_de_cadastro_de_categoria() throws Throwable {
        categoryPage.access();
    }

    @Dado("^que informo o ID \"([^\"]*)\"$")
    public void informo_o_ID(String id) throws Throwable {
        categoryPage.withID(id);
    }

    @Entao("^devem existir (\\d+) categorias$")
    public void devem_existir_categorias(int quantidadeCategorias) throws Throwable {
        Assert.assertEquals(quantidadeCategorias, managementCoursePage.getCategories().size());
    }

    @Entao("^o nome da ultima categoria deve ser \"([^\"]*)\"$")
    public void o_nome_da_ultima_categoria_deve_ser(String categoryName) throws Throwable {
        WebElement lastCategory = managementCoursePage.getLastCategory();
        String name = lastCategory.findElement(By.className("categoryname")).getAttribute("textContent");

        Assert.assertEquals(categoryName, name);
    }

    @Entao("^o ID da ultima categoria deve ser \"([^\"]*)\"$")
    public void o_ID_da_ultima_categoria_deve_ser(String categoryID) throws Throwable {
        WebElement lastCategory = managementCoursePage.getLastCategory();
        String id = lastCategory.findElement(By.className("idnumber")).getAttribute("textContent");

        Assert.assertEquals(categoryID, id);
    }

    @Dado("^que informo o nome da categoria \"([^\"]*)\"$")
    public void que_informo_o_nome_da_categoria(String nomeCategoria) throws Throwable {
        categoryPage.withName(nomeCategoria);
    }

    @Quando("^eu clicar no botão adicionar categoria$")
    public void eu_clicar_no_botão_adicionar_categoria() throws Throwable {
        categoryPage.save();
    }

    @Entao("^a mensagem \"([^\"]*)\" deve ser exibida no campo nome da categoria$")
    public void a_mensagem_deve_ser_exibida_no_campo_nome_da_categoria(String mensagem) throws Throwable {
        Assert.assertEquals(mensagem, categoryPage.getErrorMessageOnCategoryName());
    }

    @Dado("^que a categoria de nome \"([^\"]*)\" e ID \"([^\"]*)\" já existe$")
    public void que_a_categoria_de_nome_e_ID_já_existe(String nome, String id) throws Throwable {
        Category category = new Category();
        category.setNome(nome);
        category.setId(id);
        category.save();
    }

    @Entao("^a mensagem \"([^\"]*)\" deve ser exibida no campo ID da categoria$")
    public void a_mensagem_deve_ser_exibida_no_campo_ID_da_categoria(String mensagem) throws Throwable {
        Assert.assertEquals(mensagem, categoryPage.getErrorMessageOnCategoryId());
    }

}
