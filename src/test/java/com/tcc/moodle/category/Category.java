package com.tcc.moodle.category;

import com.tcc.moodle.MoodleConnection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Category {
    private String id;
    private String nome;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public void deleteAll() {
        final MoodleConnection moodleConnection = new MoodleConnection();
        
        moodleConnection.open();
        Statement stmt = null;
        
        try {
            stmt = moodleConnection.getConnection().createStatement();
            stmt.execute("DELETE FROM moodle30.mdl_course_categories WHERE id != 1");
        } catch (SQLException ex) {
            Logger.getLogger(Category.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(stmt != null) {
                
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Category.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                moodleConnection.close();
            }           
        }
        
    }
    
    public void save(){
       final MoodleConnection moodleConnection = new MoodleConnection();
        
        moodleConnection.open();
        Statement stmt = null;
        
        try {
            stmt = moodleConnection.getConnection().createStatement();
            stmt.execute("INSERT INTO moodle30.mdl_course_categories (name, idnumber, descriptionformat, sortorder) VALUES ('" + this.nome + "', '" + this.id  + "', 1, 20000)");
        } catch (SQLException ex) {
            Logger.getLogger(Category.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(stmt != null) {
                
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Category.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                moodleConnection.close();
            }           
        } 
    }

}
