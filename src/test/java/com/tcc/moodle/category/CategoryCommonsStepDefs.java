package com.tcc.moodle.category;

import com.tcc.moodle.PageConstants;
import com.tcc.moodle.SharedDriver;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Entao;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class CategoryCommonsStepDefs {

     private final WebDriver driver;

    public CategoryCommonsStepDefs(SharedDriver driver) {
        this.driver = driver;
    }
    
    @Before("@category_tests")
    public void excluir_todas_as_categorias() {
        Category category = new Category();
        category.deleteAll();
    }
    
    @Entao("^devo ser redirecionado para a pagina de administracao de cursos$")
    public void devo_ser_redirecionado_para_a_pagina_de_administracao_de_cursos() throws Throwable {
        final String URL = driver.getCurrentUrl().split("\\?")[0];
        Assert.assertEquals(PageConstants.COURSE_MANAGEMENT_URL, URL);
    }

}
