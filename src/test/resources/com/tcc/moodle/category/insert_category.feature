# language: pt
@category_tests
Funcionalidade: Cadastro de categorias

    Essa funcionalidade tem como objetivo a inserção das categorias dos cursos da plataforma Moodle.

    Regras:
        - O nome da categoria é obrigatório
        - Não podem existir categorias com o mesmo ID

    Contexto: 
        Dado que estou autenticado como administrador
        E estou na pagina de cadastro de categoria 

    Cenario: Cadastro de uma categoria
        Dado que informo o nome da categoria "EAD"
        E que informo o ID "EAD"
        Quando eu clicar no botão adicionar categoria
        Entao devo ser redirecionado para a pagina de administracao de cursos
        E devem existir 2 categorias
        E o nome da ultima categoria deve ser "EAD"
        E o ID da ultima categoria deve ser "EAD"

    Cenario: Cadastro de uma categoria sem nome
        Quando eu clicar no botão adicionar categoria
        Entao a mensagem "Necessários" deve ser exibida no campo nome da categoria

    Cenario: Cadastro de uma categoria com um ID ja existente
        Dado que a categoria de nome "EAD" e ID "EAD" já existe
        E que informo o nome da categoria "EAD"
        E que informo o ID "EAD"
        Quando eu clicar no botão adicionar categoria
        Entao a mensagem "Número de identificação já é usado para outra categoria" deve ser exibida no campo ID da categoria