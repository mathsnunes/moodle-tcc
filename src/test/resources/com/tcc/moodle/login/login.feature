# language: pt
Funcionalidade: Login do sistema

    Cenario: Login sem informar o usuario e senha
        Dado que estou na pagina de login
        Quando eu clicar no botao de login
        Entao a mensagem "Nome de usuário ou senha errados. Por favor tente outra vez." deve ser exibida

    Cenario: Login com usuario e senha incorretos
        Dado que estou na pagina de login
        E o usuario é "admin"
        E a senha é "1234"
        Quando eu clicar no botao de login
        Entao a mensagem "Nome de usuário ou senha errados. Por favor tente outra vez." deve ser exibida

    Cenario: Login como administrador do sistema
        Dado que estou na pagina de login
        E o usuario é "admin"
        E a senha é "Mo12345678_"
        Quando eu clicar no botao de login
        Entao devo ser redirecionado para a pagina principal